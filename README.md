# Job Vacancy SQL
![Iforce Consulting](img/Iforce.jpeg)

## Data Engineer (ETL Developer) at [PT. Iforce Consulting Indonesia](www.iforce.co.id) - Jakarta Raya

## Salary
IDR 12 Million - 15 Million per month

## Benefits
- Attractive salary with great benefits
- Friendly, young and dynamic environment
- Hybrid working system (3 days WFH)

## Job Description
- Perform & Maintain ETL (Extract, Transform, Load) using Microsoft SSIS ETL Tool 
- Export data report from MS SQL Server based on user requirements 
- Work closely with Developers for database design including data mapping, analysis, and validation 
- Develop script SQL (Procedure, Function, Trigger, Views, etc) for report and development purposes 
- Conduct unit tests, analyse and troubleshoot any issues related to data. 

## Requirements
- Bachelor or Diploma Degree in Computer Science/Information Technology or related major from reputable university 
- Have 3+ years' experience in Microsoft SSIS and Microsoft SQL Server 
- Knowledge in Data Warehouse & Big Data is a plus 
- Strong analytical and communication skills 
- Be able to work as a team or independently 

## Additional Information
### Job Level
Employees (non-management & non-supervisor)
### Qualification
Bachelor Degree/_Sarjana (S1)_
### Work Experience
3 years
### Work Type
Full time
### Job Specialization
Computer/Information Technology, IT-Software

[Jobstreet Link](https://www.jobstreet.co.id/id/job/data-engineer-etl-developer-4330190?jobId=jobstreet-id-job-4330190&sectionRank=19&token=0~1e13fbff-4c6b-4047-a176-2ffb9f3bac42&searchPath=%2Fid%2Fsql-developer-jobs&fr=SRP%20View%20In%20New%20Tab)
